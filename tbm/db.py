from sqlalchemy import create_engine, event, inspect
from sqlalchemy.orm import sessionmaker

from tbm.models import Base, Account, Transaction
from sqlalchemy.orm.attributes import get_history

engine = create_engine("sqlite:///tbm.db")
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)

@event.listens_for(DBSession, 'before_flush')
def receive_before_flush(session, flush_context, instances):
    """event handler to dispatch before_flush changes to models"""
    for instance in session.new:
        op = getattr(instance, "new_before_flush", None)
        if callable(op):
            instance.new_before_flush(session)
        
    for instance in session.dirty:
        op = getattr(instance, "dirty_before_flush", None)
        if callable(op):
            instance.dirty_before_flush(session)

    for instance in session.deleted:
        op = getattr(instance, "deleted_before_flush", None)
        if callable(op):
            instance.dirty_before_flush(session)

@event.listens_for(DBSession, 'after_flush')
def receive_after_flush(session, flush_context):
    """event handler to dispatch after_flush changes to models"""
    for instance in session.new:
        op = getattr(instance, "new_after_flush", None)
        if callable(op):
            instance.new_after_flush(session)
        
    for instance in session.dirty:
        op = getattr(instance, "dirty_after_flush", None)
        if callable(op):
            instance.dirty_after_flush(session)

    for instance in session.deleted:
        op = getattr(instance, "deleted_after_flush", None)
        if callable(op):
            instance.dirty_after_flush(session)

@event.listens_for(DBSession, 'after_flush_postexec')
def receive_after_flush_postexec(session, flush_context):
    """event handler to dispatch after_flush_postexec changes to models"""
    print("after_flush_postexec")
    for instance in session.new:
        op = getattr(instance, "new_after_flush_postexec", None)
        if callable(op):
            instance.new_after_flush_postexec(session)
        
    for instance in session.dirty:
        op = getattr(instance, "dirty_after_flush_postexec", None)
        if callable(op):
            instance.dirty_after_flush_postexec(session)

    for instance in session.deleted:
        op = getattr(instance, "deleted_after_flush_postexec", None)
        if callable(op):
            instance.dirty_after_flush_postexec(session)
def make_schema():
    Base.metadata.create_all(engine)


