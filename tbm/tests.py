from currency import currency_amount_to_int

print("Testing: currency_amount_to_int")
print("$4.20: {}".format(currency_amount_to_int('USD', "$4.20")))
print("$ 4.20: {}".format(currency_amount_to_int('USD', "$ 4.20")))
print("4.20 USD: {}".format(currency_amount_to_int('USD', "4.20 USD")))
print("34: {}".format(currency_amount_to_int('USD', "34")))
print(".34: {}".format(currency_amount_to_int('USD', ".34")))

