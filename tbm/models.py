import os
import sys
import datetime

from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, column_property
from sqlalchemy import create_engine
from sqlalchemy.sql import select, func

Base = declarative_base()

class AccountFolder(Base):
    __tablename__ = "account_folder"

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    parent_id = Column(Integer, ForeignKey('account_folder.id'))
    parent = relationship('AccountFolder', remote_side=[id], backref="children")
    
class Account(Base):
    __tablename__ = "account"

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    notes = Text()
    inputs = relationship("TransactionInput", back_populates="account")
    outputs = relationship("TransactionOutput", back_populates="account")
    account_folder_id = Column(Integer, ForeignKey('account_folder.id'))
    account_folder = relationship('AccountFolder', backref="accounts")

class AccountBalance(Base):
    """stores the amount each transaction adjusts account balance by"""
    __tablename__ = "account_balance"

    id = Column(Integer, primary_key=True)

    amount = Column(Integer, nullable=False)

    account_id = Column(Integer, ForeignKey('account.id'), nullable=False)
    account = relationship(Account, backref="tx_balances")

    transaction_id = Column(Integer, ForeignKey('transaction.id'), nullable=False)
    transaction = relationship("Transaction", backref="tx_balances")

class Transaction(Base):
    __tablename__ = "transaction"

    id = Column(Integer, primary_key=True)

    time = Column(DateTime(), nullable=False, default=datetime.datetime.utcnow)

    name = Column(String(250), nullable=False)

    notes = Text()


    inputs = relationship("TransactionInput")

    outputs = relationship("TransactionOutput")

    def is_complex(self):
        if len(self.inputs) > 1 or len(self.outputs) > 1:
            return True
        return False

    def new_before_flush(self, session):
        print("I'm a new transaction")
        bal_entries = {}
        for tx_input in self.inputs:
            if not tx_input.account in bal_entries:
                bal_entries[tx_input.account] = AccountBalance(account=tx_input.account, transaction=self, amount = -tx_input.amount)
            else:
                bal_entries[tx_input.account].amount -= tx_input.amount
        for tx_output in self.outputs:
            if not tx_output.account in bal_entries:
                bal_entries[tx_output.account] = AccountBalance(account=tx_output.account, transaction=self, amount = tx_output.amount)
            else:
                bal_entries[tx_output.account].amount += tx_output.amount

        for k,v in bal_entries.items():
            print("Tx: {}, Acc: {}, Amt: {}".format(self.name, k.name, v.amount))

    
class TransactionInput(Base):
    __tablename__ = "transaction_input"
    
    id = Column(Integer, primary_key=True)

    amount = Column(Integer, nullable=False)

    account_id = Column(Integer, ForeignKey('account.id'), nullable=False)
    account = relationship(Account, back_populates="inputs")

    transaction_id = Column(Integer, ForeignKey('transaction.id'), nullable=False)
    transaction = relationship(Transaction, back_populates="inputs")


class TransactionOutput(Base):
    __tablename__ = "transaction_output"
    
    id = Column(Integer, primary_key=True)

    amount = Column(Integer, nullable=False)

    account_id = Column(Integer, ForeignKey('account.id'), nullable=False)
    account = relationship(Account, back_populates="outputs")

    transaction_id = Column(Integer, ForeignKey('transaction.id'), nullable=False)
    transaction = relationship(Transaction, back_populates="outputs")


Account.output_total = column_property(
        select(func.coalesce(func.sum(TransactionOutput.amount), 0)).\
            where(TransactionOutput.account_id==Account.id).\
            correlate_except(TransactionOutput).\
            scalar_subquery()
    )
    
Account.input_total = column_property(
        select(func.coalesce(func.sum(TransactionInput.amount), 0)).\
            where(TransactionInput.account_id==Account.id).\
            correlate_except(TransactionInput).\
            scalar_subquery()
    )

Account.balance = column_property(Account.output_total - Account.input_total)

Transaction.input_total = column_property(
        select(func.coalesce(func.sum(TransactionInput.amount), 0)).\
            where(TransactionInput.transaction_id==Transaction.id).\
            correlate_except(TransactionInput).\
            scalar_subquery()
    )
Transaction.output_total = column_property(
        select(func.coalesce(func.sum(TransactionOutput.amount), 0)).\
            where(TransactionOutput.transaction_id==Transaction.id).\
            correlate_except(TransactionOutput).\
            scalar_subquery()
    )

Transaction.balanced = column_property(Transaction.input_total == Transaction.output_total)
