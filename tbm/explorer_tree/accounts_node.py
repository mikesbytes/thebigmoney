from tbm.db import DBSession
from tbm.models import AccountFolder, Account
from tbm.currency import int_to_money
from tbm import style
from PyQt5.QtWidgets import QStyle
from tbm import tbm_globals
from tbm.txview import TxView

class AccountNode(object):
    def __init__(self, account):
        self._account = account
        self._row = 0
        self._parent = None

    def data(self, column):
        if column == 0:
            return self._account.name
        elif column == 1:
            return int_to_money("USD", self._account.balance, True)

    def columnCount(self):
        return 2

    def childCount(self):
        return 0

    def child(self, row):
        return None

    def parent(self):
        return self._parent

    def row(self):
        return self._row

    def decoration(self, column):
        pass

    def double_clicked(self):
        tx_view = TxView(self._account.name)
        tx_view.set_account(self._account)
        tbm_globals.main_window.add_center_dock_widget(tx_view)

class AccountFolderNode(object):
    def __init__(self, account_folder):
        self._account_folder = account_folder
        self._row = 0
        self._parent = None
        self._children = []
        for i in self._account_folder.accounts:
            node = AccountNode(i)
            node._parent = self
            node._row = len(self._children)
            self._children.append(node)
        for i in self._account_folder.children:
            node = AccountFolderNode(i)
            node._parent = self
            node._row = len(self._children)
            self._children.append(node)

    def data(self, column):
        if column == 0:
            return self._account_folder.name

    def columnCount(self):
        return 2

    def childCount(self):
        return len(self._children)

    def child(self, row):
        return self._children[row]

    def parent(self):
        return self._parent

    def row(self):
        return self._row

    def decoration(self, column):
        pass

class AccountsNode(object):
    def __init__(self):
        self._session = DBSession()
        self._children = []

        for i in self._session.query(Account).where(Account.account_folder==None):
            node = AccountNode(i)
            node._parent = self
            node._row = len(self._children)
            self._children.append(node)

        for i in self._session.query(AccountFolder).where(AccountFolder.parent==None):
            node = AccountFolderNode(i)
            node._parent = self
            node._row = len(self._children)
            self._children.append(node)

        self._data = ["Accounts"]

        self._parent = None
        self._row = 0
        self._decorations = [style.global_style.standardIcon(getattr(QStyle, "SP_DirIcon"))]

    def data(self, column):
        if column >= 0 and column == 0:
            return "Accounts"

    def columnCount(self):
        return 2

    def childCount(self):
        return len(self._children)

    def child(self, row):
        if row >= 0 and row < self.childCount():
            return self._children[row]

    def parent(self):
        return self._parent

    def row(self):
        return self._row

    def decoration(self, column):
        if column >= 0 and column < len(self._decorations):
            return self._decorations[column]
