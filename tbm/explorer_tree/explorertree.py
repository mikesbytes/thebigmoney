from PyQt5.QtWidgets import QDockWidget, QTreeView, QStyle
from PyQtAds.QtAds import CDockWidget
from PyQt5.QtCore import Qt, QAbstractItemModel, QModelIndex
from PyQt5.QtGui import QIcon
from tbm import style
from tbm.explorer_tree.accounts_node import AccountsNode

"""
Reworked code based on
http://trevorius.com/scrapbook/uncategorized/pyqt-custom-abstractitemmodel/
Adapted to Qt5 and fixed column/row bug.
TODO: handle changing data.
"""

import sys
from PyQt5 import QtCore, QtWidgets, QtGui


class CustomNode(object):
    def __init__(self, data):
        self._data = data
        if type(data) == tuple:
            self._data = list(data)
        if type(data) is str or not hasattr(data, '__getitem__'):
            self._data = [data]

        self._columncount = len(self._data)
        self._children = []
        self._parent = None
        self._row = 0
        self._decorations = []

    def data(self, column):
        if column >= 0 and column < len(self._data):
            return self._data[column]

    def columnCount(self):
        return self._columncount

    def childCount(self):
        return len(self._children)

    def child(self, row):
        if row >= 0 and row < self.childCount():
            return self._children[row]

    def parent(self):
        return self._parent

    def row(self):
        return self._row

    def addChild(self, child):
        child._parent = self
        child._row = len(self._children)
        self._children.append(child)
        self._columncount = max(child.columnCount(), self._columncount)

    def decoration(self, column):
        if column >= 0 and column < len(self._decorations):
            return self._decorations[column]

class CustomModel(QAbstractItemModel):
    def __init__(self, nodes):
        QAbstractItemModel.__init__(self)
        self._root = CustomNode(None)
        for node in nodes:
            self._root.addChild(node)

    def rowCount(self, index):
        if index.isValid():
            return index.internalPointer().childCount()
        return self._root.childCount()

    def addChild(self, node, _parent):
        if not _parent or not _parent.isValid():
            parent = self._root
        else:
            parent = _parent.internalPointer()
        parent.addChild(node)

    def index(self, row, column, _parent=None):
        if not _parent or not _parent.isValid():
            parent = self._root
        else:
            parent = _parent.internalPointer()

        if not QAbstractItemModel.hasIndex(self, row, column, _parent):
            return QtCore.QModelIndex()

        child = parent.child(row)
        if child:
            return QAbstractItemModel.createIndex(self, row, column, child)
        else:
            return QModelIndex()

    def parent(self, index):
        if index.isValid():
            p = index.internalPointer().parent()
            if p:
                return QAbstractItemModel.createIndex(self, p.row(), 0, p)
        return QModelIndex()

    def columnCount(self, index):
        if index.isValid():
            return index.internalPointer().columnCount()
        return self._root.columnCount()

    def data(self, index, role):
        if not index.isValid():
            return None
        node = index.internalPointer()
        if role == Qt.DisplayRole:
            return node.data(index.column())
        if role == Qt.DecorationRole:
            return node.decoration(index.column())
        return None



class MyTree(QTreeView):
    """
    """
    def __init__(self, parent=None):
        QTreeView.__init__(self, parent)
        self.items = []

        self.items.append(AccountsNode())

        self.setModel(CustomModel(self.items))
        self.doubleClicked.connect(self._double_clicked)

    def appendData(self, rowItems):
        """
        TODO: how to insert data, and update tree.
        """
        model = self.getModel()
        rootIdx = model.index(0, 0, QtCore.QModelIndex())
        position = 3
        new = CustomNode("new")
        new.addChild(CustomNode(rowItems))
        model.beginInsertRows(rootIdx, position, position)
        model.addChild(new, None)
        model.endInsertRows()
        model.layoutChanged.emit()  # must call this otherwise nope!

    def _double_clicked(self, index):
        op = getattr(index.internalPointer(), "double_clicked", None)
        if callable(op):
            index.internalPointer().double_clicked()

class ExplorerTree(CDockWidget):
    def __init__(self):
        super(ExplorerTree, self).__init__("Explorer")

        self.view = MyTree()
        self.setWidget(self.view)

