currency_formats = {
    'USD':{
        'symbols': ['$','USD'],
        'fractional': True,
        'seperator': '.',
        'fraction_digits': 2
    }
}

def money_to_int(currency_type, input_str, no_symbol=False):
    cfmt = currency_formats[currency_type]

    # format input string
    input_str = input_str.strip()
    for sym in cfmt['symbols']:
        if input_str.startswith(sym):
            input_str = input_str[len(sym):]
        elif input_str.endswith(sym):
            input_str = input_str[0:-len(sym)]

    splits = input_str.split(cfmt['seperator'])
    if len(splits) == 1:
        frac = pow(10, cfmt['fraction_digits'])
        try:
            out = int(splits[0])
            return int(splits[0]) * frac
        except ValueError:
            return None
    elif len(splits) == 2:
        try:
            frac = pow(10, cfmt['fraction_digits'])
            int_part = 0
            if splits[0] != '':
                int_part = int(splits[0])
            frac_part = int(splits[1])
            if frac_part >= frac:
                return None
            return int_part * frac + frac_part
        except ValueError:
            return None
    return None

def int_to_money(currency_type, input_val, symbol=False):
    cfmt = currency_formats[currency_type]
    sep = cfmt['seperator']
    frac = pow(10, cfmt['fraction_digits'])
    if not symbol:
        return "{}{}{:02d}".format(input_val // frac, sep, input_val % frac)
    return "{}{}{}{:02d}".format(cfmt['symbols'][0], input_val // frac, sep, input_val % frac)
