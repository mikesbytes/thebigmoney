from PyQt5.QtWidgets import QTableView, QItemDelegate
from PyQt5.QtCore import Qt, QAbstractTableModel
from PyQt5.QtGui import QColor

from tbm.db import DBSession
from tbm.models import Account, Transaction, TransactionInput, TransactionOutput
from tbm.currency import money_to_int, int_to_money
from PyQtAds import QtAds
from sqlalchemy import select, func


class TxTableModel(QAbstractTableModel):
    def __init__(self, transactions, account=None):
        super(TxTableModel, self).__init__()

        self.transactions = transactions 
        self.col_map = ["id", "name", "amount"]
        self.raw_cols = ["id", "name"]
        self.editable_cols = ["name", "amount"]

        if account:
            self.account = account
            self.col_map.append("acc_amount")

        self.session = DBSession()

    def data(self, index, role):
        col_t = self.col_map[index.column()]
        tx = self.transactions[index.row()]
        if role == Qt.DisplayRole:
            if col_t in self.raw_cols:
                return getattr(self.transactions[index.row()], col_t)
            elif col_t == "amount":
                amount = self.transactions[index.row()].input_total
                return int_to_money('USD', amount)
            elif col_t == "acc_amount":
                acc_out_amt = select(func.coalesce(func.sum(TransactionOutput.amount), 0)).\
                    where((TransactionOutput.transaction_id==Transaction.id) & (TransactionOutput.account == self.account)).\
                    correlate_except(TransactionOutput)
                amount = self.session.execute(acc_out_amt).first()[0]
                acc_in_amt = select(func.coalesce(func.sum(TransactionInput.amount), 0)).\
                    where((TransactionInput.transaction_id==Transaction.id) & (TransactionInput.account == self.account)).\
                    correlate_except(TransactionInput)
                amount = amount - self.session.execute(acc_in_amt).first()[0]
                return int_to_money('USD', amount)
        elif role == Qt.ForegroundRole:
            if col_t == "amount" and tx.is_complex():
                return QColor(Qt.red)

    def rowCount(self, index):
        return len(self.transactions)

    def columnCount(self, index):
        return len(self.col_map)

    def setData(self, index, value, role):
        col_t = self.col_map[index.column()]
        if role == Qt.EditRole and col_t in self.editable_cols:
            if col_t in self.raw_cols:
                setattr(self.transactions[index.row()], col_t, value)
            elif col_t == "amount":
                amt = money_to_int("USD", value)
                if not amt:
                    return False
                self.transactions[index.row()].amount = currency_amount_to_int("USD", value)
            self.session.commit()
        return False

    def flags(self, index):
        col_t = self.col_map[index.column()]
        if col_t in self.editable_cols:
            return Qt.ItemIsSelectable|Qt.ItemIsEnabled|Qt.ItemIsEditable
        return Qt.ItemIsSelectable|Qt.ItemIsEnabled

class TxTableDelegate(QItemDelegate):
    def createEditor(self, parent, option, index):
        model = index.model()
        col_t = model.col_map[index.column()]
        if col_t in model.editable_cols:
            return super(TxTableDelegate, self).createEditor(parent, option, index)
        return None

    def setEditorData(self, editor, index):
        model = index.model()
        col_t = model.col_map[index.column()]
        if col_t in model.editable_cols:
            text = index.data(Qt.EditRole) or index.data(Qt.DisplayRole)
            editor.setText(text)
        

class TxTableView(QTableView):
    def __init__(self):
        super().__init__()
        self.setSelectionBehavior(QTableView.SelectRows)
        delegate = TxTableDelegate()
        self.setItemDelegate(delegate)


class TxView(QtAds.CDockWidget):
    def __init__(self, annote=None):
        if annote:
            super(TxView, self).__init__("Tx ({})".format(annote))
        else:
            super(TxView, self).__init__("Tx".format(annote))

        self.view = TxTableView()
        self.setWidget(self.view)
        self.session = DBSession()

    def set_account(self, account):
        transactions = self.session.query(Transaction).join(TransactionInput).join(TransactionOutput).filter((TransactionInput.account==account) | (TransactionOutput.account==account)).all()
        model = TxTableModel(transactions, account)
        self.view.setModel(model)

