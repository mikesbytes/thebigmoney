#!/usr/bin/python3

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QTableView, QItemDelegate
from PyQt5.QtCore import Qt, QAbstractTableModel
from PyQt5.QtGui import QColor

from tbm.db import DBSession
from tbm.models import Account, Transaction
from tbm.currency import money_to_int, int_to_money
from tbm.explorer_tree.explorertree import ExplorerTree
from tbm.txview import TxTableView, TxView, TxTableModel
from tbm.overview import Overview
from tbm import style
from tbm import tbm_globals

from PyQtAds.QtAds import CDockManager
from PyQtAds import QtAds

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        

        self.dock_manager = CDockManager()
        self.setCentralWidget(self.dock_manager)

        menu_bar = self.menuBar()
        menu_view = menu_bar.addMenu("View")

        # explorer tree
        self.explorer_tree = ExplorerTree()
        menu_view.addAction(self.explorer_tree.toggleViewAction())

        # overview
        self.overview = Overview()
        menu_view.addAction(self.overview.toggleViewAction())
        self.add_center_dock_widget(self.overview)

        self.dock_manager.addDockWidget(QtAds.LeftDockWidgetArea, self.explorer_tree)

    def add_center_dock_widget(self, dock_widget):
        self.dock_manager.addDockWidget(QtAds.CenterDockWidgetArea, dock_widget,
                                        self.dock_manager.lastAddedDockAreaWidget(QtAds.CenterDockWidgetArea))
    


def main():
    app = QApplication(sys.argv)
    tbm_globals.app = app
    style.global_style = app.style()
    print(style.global_style)

    window = MainWindow()
    tbm_globals.main_window = window
    window.show()

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
