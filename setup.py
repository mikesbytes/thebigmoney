from setuptools import setup

setup (
    name='thebigmoney',

    version='0.0.1',

    description="A personal finance application",

    py_modules=['tbm']
)
