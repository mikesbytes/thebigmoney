from tbm.db import DBSession, make_schema
from tbm.models import AccountFolder, Account, Transaction, TransactionInput, TransactionOutput

make_schema()

session = DBSession()

assets_folder = AccountFolder(name="Assets")

ut_acc = Account(name="Untracked")
ch_acc = Account(name="Checking", account_folder=assets_folder)

print("Initial balance tx")
ibal_tx = Transaction(name="Initial balance")
ibal_tx_in = TransactionInput(transaction=ibal_tx, account=ut_acc, amount=100000)
ibal_tx_out = TransactionOutput(transaction=ibal_tx, account=ch_acc, amount=100000)

session.add(ut_acc)
session.add(ch_acc)
session.add(ibal_tx)
session.commit()
